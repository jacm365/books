<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    private $isbnList = [
        '0005534186',
        '0978110196',
        '0978108248',
        '0978194527',
        '0978194004',
        '0978194985',
        '0978171349',
        '0978039912',
        '0978031644',
        '0978168968',
        '0978179633',
        '0978006232',
        '0978195248',
        '0978125029',
        '0978078691',
        '0978152476',
        '0978153871',
        '0978125010',
        '0593139135',
        '0441013597',
        '0553293354',
        '0345816021',
        '0593084640',
        '0671027034',
        '0143111388',
        '0735211299',
        '0517887290',
        '1544507852',
        '0316556327',
        '1524743038',
        '1984819194',
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->sentence(5, true),
            'isbn' => array_pop($this->isbnList),
            'publication_date' => $this->faker->date('Y-m-d', 1009861816),
            'status' => 'AVAILABLE',
        ];
    }
}
