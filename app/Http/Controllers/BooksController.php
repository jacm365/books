<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\User;
use App\Models\UserActionLog;
use App\Rules\Isbn;
use Illuminate\Http\Request;

class BooksController
{
    public function index()
    {
        return response([
            'books' => Book::orderBy('title', 'asc')->paginate(5)
        ]);
    }

    public function store(Request $request)
    {
        $fields = $request->validate([
            'title' => 'required|string',
            'isbn' => ['required','string', new Isbn, 'unique:books,isbn'],
            'publication_date' => 'required|date',
        ]);

        $book = Book::create([
            'title' => $fields['title'],
            'isbn' => $fields['isbn'],
            'publication_date' => $fields['publication_date']
        ]);

        $response = [
            'book' => $book
        ];

        return response($response, 201);
    }

    public function updateStatus(Request $request)
    {
        $fields = $request->validate([
            'action' => 'required|string',
            'isbn' => ['required','string', new Isbn],
        ]);

        $book = Book::where('isbn', $fields['isbn'])->first();

        if($fields['action'] === 'CHECKOUT' && $book->status === 'CHECKED_OUT') {
            return response(['message' => 'Book unavailable'], 409);
        } elseif($fields['action'] === 'CHECKIN' && $book->status === 'AVAILABLE') {
            return response(['message' => 'Book was already checked in'], 409);
        }

        $book->status = $fields['action'] === 'CHECKIN' ? 'AVAILABLE' : 'CHECKED_OUT';
        $book->save();
        UserActionLog::create([
            'book_id' => $book->id,
            'user_id' => auth()->user()->id,
            'action' => $fields['action']
        ]);

        $response = [
            'book' => $book
        ];

        return response($response);

    }
}
