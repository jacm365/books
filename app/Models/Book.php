<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'title',
        'isbn',
        'publication_date',
    ];

    public function userActionLog(): HasMany
    {
        return $this->hasMany(UserActionLog::class);
    }
}
