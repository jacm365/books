<?php

namespace Tests\Feature;

use Tests\TestCase;
use \Illuminate\Testing\TestResponse;


class UserTest extends TestCase
{
    private function registerUser($email, $password): TestResponse
    {
        $request = [
            'name' => $this->faker->name,
            'email' => $email,
            'date_of_birth' => $this->faker->date('Y-m-d', 1009861816),
            'password' => $password,
            'password_confirmation' => $password,
        ];

        return $this->post($this->baseAPI . '/users', $request, ['Accept' => 'application/json']);
    }

    /** @test */
    public function test_register_user_fails_with_malformed_request(): void
    {

        $response = $this->post($this->baseAPI . '/users', [], ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $response = $this->post($this->baseAPI . '/users', ['dummyField' => 'Some Value'], ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $request = [
            'fake_field1' => 'some text',
            'fake_field2' => 'some text',
            'fake_field3' => 'some text',
            'fake_field4' => 'some text',
        ];
        $response = $this->post($this->baseAPI . '/users', $request, ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $request = [
            'name' => '',
            'email' => '',
            'password' => 'some text',
            'password_confirmation' => 'some text',
            'date_of_birth' => 'some text',
        ];
        $response = $this->post($this->baseAPI . '/users', $request, ['Accept' => 'application/json']);
        $response->assertStatus(422);
    }

    /** @test */
    public function test_register_user_fails_password_validation(): void
    {
        $email = $this->faker->unique()->safeEmail;
        $password = 'superSecurePass'; //v
        $response = $this->registerUser($email, $password);
        $response->assertStatus(422);
    }

    /** @test */
    public function test_register_user_succeeds(): void
    {
        $email = $this->faker->unique()->safeEmail;
        $password = 'superSecurePass123';
        $response = $this->registerUser($email, $password);
        $response->assertStatus(201);
        $this->assertDatabaseHas('users', [
            'email' => $email
        ]);
    }

    /** @test */
    public function test_user_login_fails_malformed_request()
    {
        $response = $this->post($this->baseAPI . '/login', [], ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $params = [
            'fake_field1' => 'some text',
            'fake_field2' => 'some text',
        ];
        $response = $this->post($this->baseAPI . '/login', $params, ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $params = [
            'email' => 'no email format',
            'password' => '',
        ];
        $response = $this->post($this->baseAPI . '/login', $params, ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $params = [
            'email' => 'no email format',
            'password' => '',
        ];
        $response = $this->post($this->baseAPI . '/login', $params, ['Accept' => 'application/json']);
        $response->assertStatus(422);
    }

    /** @test */
    public function test_user_login_fails_wrong_credentials()
    {
        $email = $this->faker->unique()->safeEmail;
        $password = 'superSecurePass123';
        $this->registerUser($email, $password);
        $params = [
            'email' => $email,
            'password' => $password . 'wrong pass',
        ];
        $response = $this->post($this->baseAPI . '/login', $params, ['Accept' => 'application/json']);
        $response->assertStatus(401);
    }

    /** @test */
    public function test_successful_user_login_returns_token()
    {
        $email = $this->faker->unique()->safeEmail;
        $password = 'superSecurePass123';
        $registerResponse = $this->registerUser($email, $password);
        $params = [
            'email' => $email,
            'password' => $password,
        ];
        $response = $this->post($this->baseAPI . '/login', $params, ['Accept' => 'application/json']);
        $response
            ->assertStatus(201)
            ->assertJson(['user' => $registerResponse['user']])
            ->assertJsonStructure(['user', 'token']);
    }

    /** @test */
    public function test_user_logout_fails_unauthorized()
    {
        $response = $this->delete($this->baseAPI . '/logout', [], ['Accept' => 'application/json']);
        $response->assertStatus(401);
    }

    /** @test */
    public function test_user_logs_out_successfully()
    {
        $email = $this->faker->unique()->safeEmail;
        $password = 'superSecurePass123';
        $this->registerUser($email, $password);
        $params = [
            'email' => $email,
            'password' => $password,
        ];
        $loginResponse = $this->post($this->baseAPI . '/login', $params, ['Accept' => 'application/json']);

        $headers = [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $loginResponse['token']
        ];
        $response = $this->delete($this->baseAPI . '/logout', [], $headers);
        $response
            ->assertStatus(200)
            ->assertJson(['message' => 'Logged out']);
        $this->assertDatabaseMissing('personal_access_tokens', [
            'token' => $loginResponse['token']
        ]);
    }




}
