# Books Laravel API
This API contains both public and protected endpoints, for user creation and authentication, 
as well as book creation, checkin and checkout.
## Tech Requirements
- Docker
- Local ports 80 and 5432 should be free
## Tech Stack

- PHP 8.0.x
- PostgreSQL 13.2

## Setup Instructions

If you want to run this project locally please follow these steps: 
- Clone this repo
  
- Inside the folder:
```shell
vendor/bin/sail up
```
```shell
vendor/bin/sail artisan migrate:fresh
```
- If you want to populate the Books or Users with test data table use:
```shell
vendor/bin/sail artisan db:seed --class=UserSeeder
```
```shell
vendor/bin/sail artisan db:seed --class=BookSeeder
```

Check *./routes/api.php* to see the available routes.
