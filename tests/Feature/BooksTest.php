<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;
use App\Models\Book;

class BooksTest extends TestCase
{
    /** @test */
    public function test_create_book_fails_with_malformed_request()
    {
        $response = $this->post($this->baseAPI . '/books', [], ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $response = $this->post($this->baseAPI . '/books', ['dummyField' => 'Some Value'], ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $request = [
            'fake_field1' => 'some text',
            'fake_field2' => 'some text',
            'fake_field3' => 'some text',
        ];
        $response = $this->post($this->baseAPI . '/books', $request, ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $request = [
            'title' => '',
            'isbn' => '',
            'publication_date' => 'some text',
        ];
        $response = $this->post($this->baseAPI . '/books', $request, ['Accept' => 'application/json']);
        $response->assertStatus(422);
    }

    /** @test */
    public function test_create_book_succeeds()
    {
        $request = [
            'title' => 'Foundation',
            'isbn' => '0005534186',
            'publication_date' => '01/05/1942',
        ];
        $response = $this->post($this->baseAPI . '/books', $request, ['Accept' => 'application/json']);
        $response->assertStatus(201);
    }

    /** @test */
    public function test_check_action_fails_with_malformed_request()
    {
        $user = User::factory()->create();
        $headers['Accept'] = 'application/json';
        $headers['Authorization'] = 'Bearer ' . $user->createToken('testing')->plainTextToken;

        $response = $this->put($this->baseAPI . '/check', [], $headers);
        $response->assertStatus(422);

        $response = $this->put($this->baseAPI . '/check', ['dummyField' => 'Some Value'], ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $request = [
            'fake_field1' => 'some text',
            'fake_field2' => 'some text',
        ];
        $response = $this->put($this->baseAPI . '/check', $request, ['Accept' => 'application/json']);
        $response->assertStatus(422);

        $request = [
            'isbn' => '',
            'action' => '',
        ];
        $response = $this->put($this->baseAPI . '/check', $request, ['Accept' => 'application/json']);
        $response->assertStatus(422);
    }

    /** @test */
    public function test_checkout_book_fails_book_is_unavailable()
    {
        $user = User::factory()->create();
        $book = Book::factory()->create(['status' => 'CHECKED_OUT']);
        $headers['Accept'] = 'application/json';
        $headers['Authorization'] = 'Bearer ' . $user->createToken('testing')->plainTextToken;

        $request = [
            'isbn' => $book->isbn,
            'action' => 'CHECKOUT',
        ];
        $response = $this->put($this->baseAPI . '/check', $request, $headers);
        $response->assertStatus(409)
            ->assertJson(['message' => 'Book unavailable']);
    }

    /** @test */
    public function test_checkin_book_fails_book_already_checked_in()
    {
        $user = User::factory()->create();
        $book = Book::factory()->create();
        $headers['Accept'] = 'application/json';
        $headers['Authorization'] = 'Bearer ' . $user->createToken('testing')->plainTextToken;

        $request = [
            'isbn' => $book->isbn,
            'action' => 'CHECKIN',
        ];
        $response = $this->put($this->baseAPI . '/check', $request, $headers);
        $response->assertStatus(409)
            ->assertJson(['message' => 'Book was already checked in']);
    }

    /** @test */
    public function test_checkout_book_succeeds()
    {
        $user = User::factory()->create();
        $book = Book::factory()->create();
        $headers['Accept'] = 'application/json';
        $headers['Authorization'] = 'Bearer ' . $user->createToken('testing')->plainTextToken;

        $request = [
            'isbn' => $book->isbn,
            'action' => 'CHECKOUT',
        ];
        $response = $this->put($this->baseAPI . '/check', $request, $headers);
        $response->assertStatus(200);
        $this->assertEquals($response['book']['status'], 'CHECKED_OUT');
        $this->assertDatabaseHas('user_action_logs', [
            'book_id' => $book->id,
            'user_id' => $user->id,
            'action' => 'CHECKOUT',
        ]);
    }

    /** @test */
    public function test_retrieve_paginated_books_fails_authorization()
    {
        $response = $this->get($this->baseAPI . '/books', ['Accept' => 'application/json']);
        $response->assertStatus(401);
    }

    /** @test */
    public function test_retrieve_paginated_books_succeeds()
    {
        $user = User::factory()->create();
        Book::factory()->count(15)->create();
        $headers['Accept'] = 'application/json';
        $headers['Authorization'] = 'Bearer ' . $user->createToken('testing')->plainTextToken;
        $response = $this->get($this->baseAPI . '/books', $headers);
        $response->assertStatus(200)
            ->assertJsonStructure([
                'books' => [
                    'current_page', 'data', 'from', 'last_page', 'per_page', 'to', 'total'
                ]
            ])
            ->assertJson([
                'books' => [
                    'last_page' => 3,
                    'per_page' => 5,
                    'total' => 15
                ]
            ]);
    }
}
