<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Rules\Isbn;

class IsbnTest extends TestCase
{
    /** @test  */
    public function test_invalid_isbn_fails()
    {
        $rule = new Isbn;
        $this->assertFalse($rule->passes('isbn', '2205534186'));
        $this->assertFalse($rule->passes('isbn', 'stringof10'));
        $this->assertFalse($rule->passes('isbn', '8749488794'));
        $this->assertFalse($rule->passes('isbn', '123456789Y'));
    }

    /** @test */
    public function test_valid_isbn_returns_true()
    {
        $rule = new Isbn;
        $this->assertTrue($rule->passes('isbn', '0005534186'));
        $this->assertTrue($rule->passes('isbn', '0978110196'));
        $this->assertTrue($rule->passes('isbn', '0978108248'));
        $this->assertTrue($rule->passes('isbn', '0978194527'));
        $this->assertTrue($rule->passes('isbn', '0978194004'));
    }
}
