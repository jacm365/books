<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Isbn implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if( is_string($value) === false ||
            strlen($value) != 10 ||
            preg_match('/\d{9}[0-9xX]/i', $value) == false ) {
            return false;
        }
        $check = 0;
        for($i = 0; $i < 10; $i++) {
            if (strtoupper($value[$i]) === 'X') {
                $check += 10 * intval(10 - $i);
            } else {
                $check += intval($value[$i]) * intval(10 - $i);
            }
        }

        return $check % 11 === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'The :attribute is not a valid ISBN code.';
    }
}
